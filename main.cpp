// Napisać program umieszczający na liście jednokierunkowej G (stała) losowo wygenerowanych liczb z zakresu od 0 do stałej N.
// Następnie usunąć z listy wszystkie liczby mniejsze od średniej wartości liczb z początkowej listy,
// a potem zamienić pierwszy element listy z przedostatnim (w przypadku listy jedno i dwuelementowej nie robić nic).
// Należy wydrukować listę przed i po usuwaniu liczb oraz po zamianie.

using namespace std;
#include <iostream>
#include <ctime>
#include <cstdlib>

struct OneWayList {
    int value;
    OneWayList* next;
};

const int N = 40;
const int G = 30;

int getRandomInt(int limit) {
    return rand() % (limit + 1);
}

void append(OneWayList *&listHead, OneWayList *&listTail, int value) {
    OneWayList *newListElement = new OneWayList;
    newListElement->value = value;
    newListElement->next = NULL;
    if(listHead == NULL) {
        listHead = newListElement;
        listTail = newListElement;
    } else {
        listTail->next = newListElement;
        listTail = newListElement;
    }
}

float getAvg(OneWayList *listHead) {
    float total = 0;
    float totalCount = 0;
    OneWayList *newHead;
    newHead = listHead;
    while (newHead!=NULL) {
        totalCount++;
        total = total + (float)newHead->value;
        newHead = newHead->next;
    }
    return total / totalCount;
}

void print(OneWayList *adres) {
    OneWayList *printHead;
    printHead = adres;
    while (printHead!=NULL) {
        cout << printHead->value << (printHead->next != NULL ? ", " : "");
        printHead = printHead->next;
    }
}

void removeIfLessThan(OneWayList *&listHead, float minValue) {
    OneWayList *currentHead, *beforeHead;

    beforeHead = NULL;
    currentHead = listHead;

    while (currentHead != NULL) {

        if((float)currentHead->value < minValue) {
            OneWayList *tmp;
            if(currentHead == listHead) {
                listHead = listHead->next;
            } else {
                beforeHead->next = currentHead->next;
            }

            tmp = currentHead;
            currentHead = currentHead->next;
            delete tmp;

        } else {
            beforeHead = currentHead;
            currentHead = currentHead->next;
        }
    }
}

void swapFirstAndLastButOne(OneWayList *& listHead) {
    OneWayList *first, *second, *last, *lastButOne, *lastButTwo, *current;
    current = listHead;
    first = listHead;
    second = listHead->next;

    while (current->next->next!=NULL) {
        last = current->next->next;
        lastButOne = current->next;
        lastButTwo = current;
        current = current->next;
    }

    lastButOne->next = second;
    listHead = lastButOne;
    lastButTwo->next = first;
    first->next = last;

}

int main() {
    srand(time(NULL));

    OneWayList *head = NULL, *tail = NULL;

    for (int i = 0; i < G; ++i) append(head, tail, getRandomInt(N));
    print(head);

    float avg = getAvg(head);
    cout << endl << "avg: " << avg << endl;
    removeIfLessThan(head, avg);
    print(head);

    if (head == NULL || head->next == NULL || head->next->next == NULL) return 0;

    cout << endl << "swap: " << endl;
    swapFirstAndLastButOne(head);
    print(head);

    cout << endl;
    return 0;
}

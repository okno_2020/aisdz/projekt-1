# Temat p1

Napisać program umieszczający na liście jednokierunkowej G (stała) losowo wygenerowanych liczb z zakresu od 0 do stałej N.

Następnie usunąć z listy wszystkie liczby mniejsze od średniej wartości liczb z początkowej listy, a potem zamienić pierwszy element listy z przedostatnim (w przypadku listy jedno i dwuelementowej nie robić nic).

Należy wydrukować listę przed i po usuwaniu liczb oraz po zamianie.
